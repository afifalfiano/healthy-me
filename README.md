# HealthyMe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

## Requirements
1. NodeJS version 18.14.0
2. Angular 16.1.0
3. Angular Service Worker 16.1.0
4. Nguniversal Express Engine 16.2.0

## Feature
1. Responsive UI
2. Shared Components
3. Angular SSR
4. Angular PWA
5. Modules SCSS
6. All of the style build by native and not yet using third party like bootstrap, tailwind, or etc. I prefer to use this way to make sure that my skill still relevant to create custom UI and not depends by framework css.

## How To Run Project

1. First of all, you need to run `npm install`` to ensure that the project already install all package.
2. Next, if you want to serve as a client side just run `npm run start`, otherwise you can run `npm run dev:ssr` as a serve side.
4. Then, just open url `http://localhost:4466` to show the application on the browser.
3. Finally, you can see the different client side and server side. Moreover you can also try to using the PWA feature.

## Report by Lighthouse

1. Report Desktop
![Report Desktop](./previews/report-dekstop.png)


2. Report Mobile
![Report Mobile](./previews/report-mobile.png)


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
