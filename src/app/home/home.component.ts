import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StrongPasswordRegx } from '../shared/common/constant';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  registerForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required, Validators.pattern(StrongPasswordRegx)]),
    age: new FormControl('', [Validators.required, Validators.min(12)]),
    height: new FormControl('', [Validators.required]),
    weight: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
  })

  dataHeight: number[] = new Array(200);
  dataWeight: number[] = new Array(100);


  get emailControl(): any {
    return this.registerForm.get('email');
  }

  get passwordControl(): any {
    return this.registerForm.get('password');
  }

  get ageControl(): any {
    return this.registerForm.get('age');
  }

  get heightControl(): any {
    return this.registerForm.get('height');
  }

  get weightControl(): any {
    return this.registerForm.get('weight');
  }

  get genderControl(): any {
    return this.registerForm.get('gender');
  }

  goToGoogle(): void {
    window.open('https://google.com', '_blank', 'noopener');
  }

  goToFacebook(): void {
    window.open('https://facebook.com', '_blank', 'noopener');
  }

  goToApple(): void {
    window.open('https://apple.com', '_blank', 'noopener');
  }

  onBtnRegister(): void {
    if (this.registerForm.valid) {
      return window.alert(JSON.stringify(this.registerForm.value));
    }

    return this.registerForm.markAllAsTouched();
  }

  goToLogin(): void {
    window.alert("On the way to the login page");
  }

  onBtnClickHelp(): void {
    window.alert('Do you have any questions?');
  }
}
