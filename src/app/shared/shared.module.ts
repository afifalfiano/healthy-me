import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HmNavbarComponent } from './components/hm-navbar/hm-navbar.component';
import { HmContainerComponent } from './components/hm-container/hm-container.component';
import { RouterModule } from '@angular/router';


const sharedComponents = [
  HmNavbarComponent,
  HmContainerComponent
]


@NgModule({
  declarations: [
    ...sharedComponents,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    ...sharedComponents
  ]
})
export class SharedModule { }
