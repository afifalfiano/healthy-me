import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HmNavbarComponent } from './hm-navbar.component';

describe('HmNavbarComponent', () => {
  let component: HmNavbarComponent;
  let fixture: ComponentFixture<HmNavbarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HmNavbarComponent]
    });
    fixture = TestBed.createComponent(HmNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
