import { Component } from '@angular/core';

@Component({
  selector: 'hm-navbar',
  templateUrl: './hm-navbar.component.html',
  styleUrls: ['./hm-navbar.component.scss']
})
export class HmNavbarComponent {

  goToLogin(): void {
    window.alert('On the way to the login page');
  }

  goToRegister(): void {
    window.alert("You're on the register page")
  }
}
