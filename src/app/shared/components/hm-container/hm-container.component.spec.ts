import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HmContainerComponent } from './hm-container.component';

describe('HmContainerComponent', () => {
  let component: HmContainerComponent;
  let fixture: ComponentFixture<HmContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HmContainerComponent]
    });
    fixture = TestBed.createComponent(HmContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
